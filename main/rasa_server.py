# -*- coding: utf-8 -*-
# Author: Roy
import sys
sys.path.append('../')
sys.path.append("../configs/")
from rasa.core.agent import Agent
from rasa.core.interpreter import RasaNLUInterpreter
from rasa.core.utils import EndpointConfig
from custom_connector import HIN
from rasa.core.tracker_store import RedisTrackerStore
import json
import os
from db.tracker_store_connection import load_config
from logger_conf.logger import get_logger, CustomAdapter
from raven import Client
# chan is the custom channel connector required to handle input and output of the rasa stack

import spacy

logger = get_logger(__name__)  # name of the module
logger = CustomAdapter(logger, {"sender_id": None})

# loading endpoints
gen_config = load_config()
# sentry connection
client = Client(gen_config["raven"]["url"])

with open("../configs/parameters.json","r", encoding="utf-8") as fa:
    parameters = json.load(fa)


## redis connection for trackers
redis_config = load_config()
redis_db_no = redis_config['redis_tracker_store']['db_tracker']

environment = redis_config["server"]["environment"]
if environment == "prod":
    # Reading from environment variables in case of Prod
    redis_host = os.environ["redis_host"]
    redis_port = os.environ["redis_port"]
        
    spacy_model_obj = spacy.load("/word_vector/kn_vectors_wiki_lg")
    
else:
    redis_host = redis_config["redis_tracker_store"]["url"]
    redis_port = redis_config["redis_tracker_store"]["port"]


def serve(port, i):

    try: 
        chan = HIN()
        track_store = RedisTrackerStore(domain="default", host=redis_host, port=redis_port, db= redis_db_no, record_exp=32400)
       

        logger.info("\nLoading NLU Model\n")
        logger.info("Environment is : "+str(environment))
        if environment == "prod":
            nlu_interpreter = RasaNLUInterpreter(parameters["nlu"],spacy_model_obj=spacy_model_obj)
            action_url = "http://"+ os.environ["ACTION_SERVER"] + "/webhook"
            logger.info("Action url is : "+str(action_url))
            act = EndpointConfig(url=action_url)
        else:
            nlu_interpreter = RasaNLUInterpreter(parameters["nlu"])
            act = EndpointConfig(url=parameters["action_server"])
        
        logger.info("\nNLU Model Loaded\n")
        logger.info(f"Hitting to actions : {act.url}")

        agent = Agent.load(parameters["core"], interpreter = nlu_interpreter, action_endpoint=act,tracker_store=track_store)

        logger.info("\n Core Model Loaded and Ready to Serve!\n")
        logger.info("="*100)

        logger.info("Started on port -> " + str(port))
        agent.handle_channels([chan], int(port))
    
    except Exception as e:
        logger.info("ERROR!!! Not able to load Model: "+str(e))
        client.captureException()

if __name__ == '__main__':
    import datetime
    import multiprocessing
    import os
    import socket
    import subprocess
    import threading
    import time
    from concurrent import futures
    from itertools import cycle
    import psutil
    import requests
    from pytz import timezone

    ps = psutil.Process()
    cpul = ps.cpu_affinity()  # CPU affinity list for the process
    if environment == "prod":
        no_cpu = os.environ["no_process"]
        first_port = 9001
    else:
        no_cpu = 1
        first_port = 5002
    
    ports = []
    for i in range(int(no_cpu)):
        portnum = first_port + i
        ports.append(str(portnum))
    port_pool = cycle(ports)

    if (len(cpul) == int(no_cpu)):
        # Exclusiveness in set. Bind to cpu list
        for i in cpul:
            p = multiprocessing.Process(target=serve, args=(int(next(port_pool)), i))
            p.start()
    else:
        # No exclusiveness. Bind to first "no_cpu" cpus.
        for i in range(0, int(no_cpu)):
            p = multiprocessing.Process(target=serve, args=(int(next(port_pool)), i))
            p.start()
