# -*- coding: utf-8 -*-
# Author: Roy
import sys
sys.path.append('../')
import asyncio
import os
import inspect
import json
import logging
import pytz
import pickle
import time
from datetime import datetime, timezone
from datetime import date, timedelta
from typing import Text, List, Dict, Any, Optional, Callable, Iterable, Awaitable
from operator import itemgetter
from asyncio import Queue, CancelledError
from sanic import Sanic, Blueprint, response
from sanic.request import Request
from sanic.response import HTTPResponse
from sanic import response
import rasa.utils.endpoints
from rasa.core.channels.channel import UserMessage, QueueOutputChannel
from rasa.core.channels.channel import InputChannel
from rasa.core.channels.channel import CollectingOutputChannel
from rasa.core import utils
from utils.email_alert import send_mail as alert
from utils.email_alert import send_converstaion_logs

from utils.text_format import color
from threading import Thread
from db.tracker_store_connection import load_config, MongoDB, RedisDB
from threading import Thread
from raven import Client

from logger_conf.logger import get_logger, CustomAdapter

logger = get_logger(__name__) 
logger = CustomAdapter(logger, {"sender_id": None})

# time in IST  
utc_dt = datetime.now(timezone.utc)
IST = pytz.timezone('Asia/Kolkata')


# fetch the messages from the template file 
with open("../configs/templates_english.json", "r", encoding="utf-8") as temp:
    templates = json.load(temp)

# loading endpoints
gen_config = load_config()
# redis connection
client = Client(gen_config["raven"]["url"])

mongo_conn = MongoDB()
redis_db_obj = RedisDB()

## colors and text format
red = color.RED
blue = color.BLUE
cyan = color.CYAN
darkcyan = color.DARKCYAN
green = color.GREEN
purple= color.PURPLE
yellow = color.YELLOW
bold = color.BOLD
underline = color.UNDERLINE
end = color.END

def alert_me(msg, key="", wait=False):
    t = Thread(target=send_converstaion_logs, args=(msg,key))
    t.start()
    if wait:
        t.join()

header_html = "<h3> Jarvis - Karnataka Survey</h3>"
header_html_2 = "<h4> Sender id : s_id , Mobile Number : m_no </h4>"  





#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class HIN(InputChannel):
    """A custom http input channel.

    This implementation is the basis for a custom implementation of a chat
    frontend. You can customize this to send messages to Rasa Core and
    retrieve responses from the agent."""

    @classmethod
    def name(cls) -> Text:
        return "rest"

    @staticmethod
    async def on_message_wrapper(
        on_new_message: Callable[[UserMessage], Awaitable[Any]],
        text: Text,
        queue: Queue,
        sender_id: Text,
        input_channel: Text,
        metadata: Optional[Dict[Text, Any]],
    ) -> None:
        collector = QueueOutputChannel(queue)

        message = UserMessage(
            text, collector, sender_id, input_channel=input_channel, metadata=metadata
        )
        await on_new_message(message)

        await queue.put("DONE")  # pytype: disable=bad-return-type

    async def _extract_sender(self, req: Request) -> Optional[Text]:
        return req.json.get("sender", None)

    # noinspection PyMethodMayBeStatic
    def _extract_message(self, req: Request) -> Optional[Text]:
        return req.json.get("message", None)

    def _extract_input_channel(self, req: Request) -> Text:
        return req.json.get("input_channel") or self.name()

    def _extract_mobile(self, req: Request) -> Text:
        return req.json.get("mobile",None )or self.name()

    def _extract_id(self, req: Request) -> Optional[Text]:
        logger.info(f" Request--{req.json}")
        return req.json.get("_id", "")


    def stream_response(
        self,
        on_new_message: Callable[[UserMessage], Awaitable[None]],
        text: Text,
        sender_id: Text,
        input_channel: Text,
        metadata: Optional[Dict[Text, Any]],
    ) -> Callable[[Any], Awaitable[None]]:
        async def stream(resp: Any) -> None:
            q = Queue()
            task = asyncio.ensure_future(
                self.on_message_wrapper(
                    on_new_message, text, q, sender_id, input_channel, metadata
                )
            )
            result = None  # declare variable up front to avoid pytype error
            while True:
                result = await q.get()
                if result == "DONE":
                    break
                else:
                    await resp.write(json.dumps(result) + "\n")
            await task

        return stream  # pytype: disable=bad-return-type

    def blueprint(
        self, on_new_message: Callable[[UserMessage], Awaitable[None]]
    ) -> Blueprint:
        nlp_vb_webhook = Blueprint(
            "nlp_vb_webhook_{}".format(type(self).__name__),
            inspect.getmodule(self).__name__,
        )

        # noinspection PyUnusedLocal
        @nlp_vb_webhook.route("/", methods=["GET"])
        async def health(request: Request) -> HTTPResponse:
            return response.json({"status": "ok"})
        
        @nlp_vb_webhook.route("/livecheck", methods=["GET"])
        async def livecheck(request: Request) -> HTTPResponse:
            logger.info("==++=livecheck=++==")
            return response.json({"status": "ok"})

        @nlp_vb_webhook.route("/readycheck", methods=["GET"])
        async def readycheck(request: Request) -> HTTPResponse:
            logger.info("==++=readycheck=++==")
            return response.json({"status": "ok"})
        

        @nlp_vb_webhook.route("/initiate", methods=["POST"])
        async def initiate(request):
            try:
                user_details = request.json
                logger.info("Recieved the call request for inserting to DB : "+str(user_details))
                existing_details  = {
                                    "full_name" : user_details['name'],
                                    "phone_number" : int(user_details['phone']),
                                    "language" : "english",
                                    "main_ans_seq" : 0,
                                    "main_seq_no" : 0,
                                    "main_no_ans_seq" : 0,
                                    "dscn_count": 0,
                                    "call_status" : "",
                                    "sts" : "",
                                    "flow_id" : "",
                                    "stage":"",
                                    "call_comp_flag" : "",
                                    "STAGE_CODE":"",
                                    "stage" : "",
                                     }
                condition = {"phone_number": int(user_details['phone'])}
                if '_id' in user_details and user_details['_id']:
                    _id=str(user_details['_id'])
                    condition = {'_id':str(_id)}
                    upd_cond=condition
                    logger.info("*************Condition Updated***************")
                else:
                    upd_cond = {"phone_number" : int(user_details['phone'])} # condition on which the update must happen
                    
                file_count = mongo_conn.mongo_file_count(col_type="input",q1=condition) 
                logger.debug("custom_connector: file count: "+str(file_count))
                if int(file_count) >= 1:
                    is_updated = mongo_conn.mongo_update(col_type="input",record={"language":"english" },upd_cond=upd_cond)

                    if is_updated is True:
                        logger.info("Updated the input collection with the update sequence")
                    else:
                        logger.info(" Failed to update in the input collection")
                else:
                    logger.info("Adding a new record for : "+str(user_details['phone']))
                    is_inserted = mongo_conn.mongo_insert(col_type="input",record=existing_details)

                    if is_inserted is True:
                        logger.info("INSERTED IN REPORT DB ")
                    else:
                        logger.info("Failed to insert in Report DB. Please see logs for errors ")

                status_update = {"call_status":"Updated Call Status Successfully"}
                logger.info("Sending the response: "+str(status_update))
                return response.json(status_update)
            except:
                logger.exception("Exception!! When inserting the new details to DB")
                status_update = {"initiate":"Inserting new details Failed!"}
                return response.json(status_update)    



        @nlp_vb_webhook.route("/webhook", methods=["POST"])
        async def receive(request: Request) -> HTTPResponse:
            logger.info("="*100)
            logger.info(" Entered webhook method ")
            start_time = time.time()   
            try:
                sender_id = str(await self._extract_sender(request))
                input_channel = self._extract_input_channel(request)
                metadata = self.get_metadata(request)
                asr_text = str(self._extract_message(request))
                mobile = int(self._extract_mobile(request))
                _id = str(self._extract_id(request))
                logger.info("custom_connector: Object_id: "+str(_id),sender_id = sender_id)
                logger.info("custom_connector: sender_id: "+str(sender_id),sender_id = sender_id)
                logger.info("custom_connector: phone_number: "+str(mobile),sender_id = sender_id)
                logger.info("custom_connector: ASR_text:====>> [ "+str(asr_text)+" ]",sender_id = sender_id)
           
                end_time = time.time()
                response_time = end_time - start_time
                logger.info("After Extracting the request  : " + " " + " || Response time : "+ str(response_time), sender_id = sender_id)  

                    
            except Exception as e:
                logger.exception("Exception in webhook for none value fo sender_id or mobile" + str(e),sender_id = sender_id)
                client.captureException()
                ## if sender_id id None end the call    
                endMessage = templates["utter_bot_not_understand"]   
                return response.json(endMessage.encode('utf-8').strip().decode("utf-8", "ignore"))
                
            res = []

            # ASR text -  handle none
            if asr_text is None or asr_text == '' or asr_text == " ":
                # asr_text = templates['set_asr_text']
                asr_text = "can you repeat again"

            should_use_stream = rasa.utils.endpoints.bool_arg(
                request, "stream", default=False
            )


            # Logic to fetch the data from the mongo and push it to redis
            try:
                # Connect to the Redis Database and check if the number is already preset or else load the mongo Database and import to Redis
                # check if the sender-id mobile number mapping is done             
                end_time = time.time()
                response_time = end_time - start_time
                logger.info("Before getting dict from redis  : " + " " + " || Response time : "+ str(response_time), sender_id = sender_id)             

                redis_data = redis_db_obj.redis_get_dict_value(str(sender_id))              
                end_time = time.time()
                response_time = end_time - start_time
                logger.info("After getting dict from redis  : " + " " + " || Response time : "+ str(response_time), sender_id = sender_id)            


                logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")              
                logger.info("\n Latest redis data from webhook:::-->  "+str(redis_data)+"\n", sender_id = sender_id)              
                logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++") 

                if redis_data is None:
                    logger.debug(f"Session Start")
                    # Load the Database configuration
                    logger.debug("custom_connector: Before fething records ", sender_id = sender_id)
                          
                    end_time = time.time()
                    response_time = end_time - start_time
                    logger.info("If redis data is none ->  file_count > 0  --> before mongo_get_collection_data : find_one: " + " " + " || Response time : "+green+underline+ str(response_time)+end, sender_id = sender_id)  

                    condition = {"customerCRTId":str(sender_id)}
                    q1 = condition # filter condition to find the document
                    q2 = {"_id": 0}  # 2nd query condition optional, setting id column to 0 , or if only certain fields are req  OPTIONAL
                    input_collection = mongo_conn.mongo_get_collection_data(col_type="input",q1=q1,q2=q2)
                              
                    end_time = time.time()
                    response_time = end_time - start_time
                    logger.info("If redis data is none ->  file_count > 0  --> After mongo_get_collection_data : find_one  : " + " " + " || Response time : "+green+underline+ str(response_time)+end, sender_id = sender_id)  
 
                    # file_count = mongo_conn.mongo_file_count(col_type="input",q1=condition) 
                    # logger.debug("custom_connector: file count: "+str(file_count))
                    if input_collection is not None:
                        
                        logger.info("\n Input collection obtained --> "+str(input_collection)+"\n")
                        input_collection = mongo_conn.mongo_add_required_keys(input_collection)
                        
                        end_time = time.time()
                        response_time = end_time - start_time
                        logger.info("If redis data is none ->  file_count > 0 -> mapping redis value with senderid   : " + " " + " || Response time : "+ str(response_time), sender_id = sender_id)  

                        # mapping phone number with dictionary from mongo database
                        logger.info("mapping sender ID with dictionary from mongo database")
                        val = redis_db_obj.redis_set_dict_value_and_expiry(str(sender_id),input_collection,32400)     #SETTING THE DICTIONARY VALUES USING PICKLE
                        logger.info("return val from redis set dict val --> "+str(val))
          
                        end_time = time.time()
                        response_time = end_time - start_time
                        logger.info("If redis data is none ->  file_count > 0 -> after writing readed customer data to redis with senderid   : " + " " + " || Response time : "+ str(response_time), sender_id = sender_id)  

                        
                    else:
                        ## if phone_number is not in mongo collection
                        res.append(templates['utter_bot_not_understand'])
                        return response.json(res[0].encode('utf-8').strip().decode("utf-8", "ignore"))
                        # return response.json(res.strip())
                else:
                    pass
            except Exception as e:
                logger.exception("custom_connector: Error!!! when fetching database and setting in redis:"+str(e))
                client.captureException()

          
            end_time = time.time()
            response_time = end_time - start_time
            logger.info("before hiting rasa  : " + " " + " || Response time : "+ str(response_time), sender_id = sender_id)  


            try:
                if should_use_stream:
                    return response.stream(
                        self.stream_response(
                            on_new_message, asr_text, sender_id, input_channel, metadata
                        ),
                        content_type="text/event-stream",
                    )
                else:
                    collector = CollectingOutputChannel()
                    # noinspection PyBroadException
                    try:
                        await on_new_message(
                            UserMessage(
                                asr_text,
                                collector,
                                sender_id,
                                input_channel=input_channel,
                                metadata=metadata,
                            )
                        )
                    except CancelledError:
                        logger.exception(
                            "Message handling timed out for "
                            "user message '{}'.".format(asr_text)
                        )
                    except Exception:
                        logger.exception(
                            "An exception occured while handling "
                            "user message '{}'.".format(asr_text)
                        )
                    
                    try:
                        res = list(map(itemgetter('text'), collector.messages))

                        end_time = time.time()
                        response_time = end_time - start_time
                        logger.info(" when rasa replied us   : " + " " + " || Response time : "+ str(response_time), sender_id = sender_id)  

                        # If Core is not able to predict the next action
                        if len(res) < 1 or res is None:
                            res.append(templates['utter_bot_not_understand'])
                        else:
                            pass

                    except Exception as e:
                        logger.exception("custom_connector: Error!! Collector messages not received: "+str(e))
                        res.append(templates['utter_bot_not_understand'])
                        alert(str(e))

                    logger.info("Reply from action server : "+str(res))

                    if "fallback_word" in res[0]:
                        res[0] = res[0].replace("fallback_word", "")
                        
                    # Code for Saving the Conversation Transcription
                    try:
                        
                        current_date = datetime.now().strftime("%Y-%m-%d")
                        log_dict = {"asr_transcript":asr_text,"bot_response":res[0].replace('EOC','').replace('TTA','')}

                        end_time = time.time()
                        response_time = end_time - start_time
                        logger.info(" Saving the Conversation  before reading redis data: " + " " + " || Response time : "+str(response_time), sender_id = sender_id)  
  

                        update_dict = redis_db_obj.redis_get_dict_value(str(sender_id))

                        end_time = time.time()
                        response_time = end_time - start_time
                        logger.info(" Saving the Conversation  after reading redis data: " + " " + " || Response time : "+ str(response_time), sender_id = sender_id)  

                        if update_dict is not None:

                            if "conversation_log" in update_dict.keys():
                                log_dict["confidence"] = update_dict["tracker_log"][-1]["confidence"]
                                log_dict["intent"] = update_dict["tracker_log"][-1]["intent"]
                                update_dict['conversation_log'].append(log_dict)
                                redis_db_obj.redis_set_dict_value_and_expiry(str(sender_id),update_dict,32400)
                                
                            else:
                                logger.info("---------------conversation_log key is not present-----------------", sender_id = sender_id)
                                pass

                        else:
                            logger.info("None type dict for sender_id"+str(sender_id)+"when writing converstion logs", sender_id = sender_id)
                            pass
                
                        
                        end_time = time.time()
                        response_time = end_time - start_time
                        logger.info(" Saving the Conversation  after updating redis data with conversation logs: " + " " + " || Response time : "+ str(response_time), sender_id = sender_id)  


                    except Exception as e:
                        client.captureException()
                        logger.exception("Error When Writing Conversation Logs in the Custom Connector!!!"+str(e), sender_id = sender_id)

                    
                    end_time = time.time()
                    response_time = end_time - start_time
                    logger.info("Sending Response : "+res[0]+ " || Response time : "+ str(response_time), sender_id = sender_id)
                    
                    logger.info(f"exiting webhook method Sender ID {sender_id}")
                    res[0] = res[0].replace(",","~")
                    
                    return response.json(res[0].encode('utf-8').strip().decode("utf-8", "ignore"))
           
            except Exception as e:
                logger.exception('Exception when sending response:'+str(e))
                client.captureException()

        return nlp_vb_webhook