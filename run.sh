#! /bin/bash
DATE=`/bin/date '+%Y-%m-%d-%H-%M-%S'`

LOG=OLD_LOGS
CONVO_LOG=convo_logs
if [ ! -d "$LOG" ]; then
    mkdir $LOG
fi

if [ ! -d "$CONVO_LOG" ]; then
    mkdir $CONVO_LOG
fi

pkill -f -9 python3
export LANG=C.UTF-8
export BOT_ENV="dev"
cd main/
mv nohup.out ../$LOG/nohup.out-${DATE}.log
pkill -f -9 python3
#nohup rasa run --endpoints ../configs/endpoints.yml --credentials ../configs/credentials.yml --connector custom_connector.HindiTVS --model ../models/ --port 5002 >> nohup.out 2>&1 &
nohup python3 rasa_server.py >> nohup.out 2>&1 &

 
