import boto3
import json
import re

def get_bucket_audio_files(bucket_name, folder_name):

    s3= boto3.resource('s3')

    bucket = s3.Bucket(bucket_name)

    bucket_audio_files = []
    print("Checking Audio files from and S3 bucket and templates.....")
    for obj in bucket.objects.all():
        if obj.key.startswith(folder_name):
            bucket_audio_files.append(obj.key.split("/")[-1])
            
    return bucket_audio_files

def get_template_audio_files(template_file):
    print(template_file)
    with open(template_file, "r", encoding="utf-8") as temp:
        data = json.load(temp)
    values=list(data.values())
    template_audio_files = []
    for value in values:
        template_audio_files.extend(re.findall('[a-zA-Z0-9_]+.wav', value))
    return template_audio_files

def get_missing_audio_files(template_audio_files, bucket_audio_files):
    
    missing_audio_files = []
    for audio_file in template_audio_files:
        if audio_file not in bucket_audio_files:
            missing_audio_files.append(audio_file)
        raw_file = audio_file.replace('.wav', '.raw')
        if raw_file not in bucket_audio_files:
            missing_audio_files.append(raw_file)
    print("Completed.")
    return missing_audio_files
