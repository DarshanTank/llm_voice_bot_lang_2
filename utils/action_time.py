import functools
import time as timer
from logger_conf.logger import get_logger, CustomAdapter
from utils.text_format import color

purple = color.PURPLE
logger = get_logger(__name__)
logger = CustomAdapter(logger, {"sender_id": None})


def timeit(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        logger.info(purple+ "in time function")
        start_time = timer.perf_counter()
        value = func(*args, **kwargs)
        end_time = timer.perf_counter()
        run_time = end_time - start_time
        # logger.debug(str(func))
        # logger.debug(func.__class__)
        function_name = str(func.__class__.__name__)
        function_name = str(func).split("at")[0]
        function_name = function_name.replace("<function","")
        logger.info( f"{function_name} took {round(run_time, 3)} secs".center(100, "*"))
        return value
    return wrapper
