# -*- coding: utf-8 -*-
import sys
import requests
import re
sys.path.append('../')
from flask import Flask
from flask_mail import Mail, Message

from logger_conf.logger import get_logger, CustomAdapter
logger = get_logger(__name__)
logger = CustomAdapter(logger, {"sender_id": None})


from db.tracker_store_connection import load_config
email_config = load_config()


# from cfg.conf import get_conf

# Getting required details from config file.
async_mode = None
app = Flask('GNANI_API_SERVICE')

##------------gmail config ------------##
# app.config['SECRET_KEY'] = 'secret!'
# app.config['MAIL_SERVER'] = "smtp.gmail.com"
# app.config['MAIL_PORT'] = 465
# app.config['MAIL_USERNAME'] = "api.service@gnani.ai"
# app.config['MAIL_PASSWORD'] = "apiservice@123"
# app.config['MAIL_USE_TLS'] = False
# app.config['MAIL_USE_SSL'] = True

##************ outlook config***********##
app.config['SECRET_KEY'] = 'secret!'
app.config['MAIL_SERVER'] = "smtp.office365.com"
app.config['MAIL_PORT'] = 587
app.config['MAIL_USERNAME'] = "vb.services@gnani.ai"
app.config['MAIL_PASSWORD'] = "Vag516734"
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
mail = Mail(app)

ops_team = email_config["mail"]["mail_ops_team"]

def send_mail(message, key=""):
    """
    send email
    :param auth_key: secure way of email communication
    :param email_id: email id input
    :param access_key: access key as input
    :return:
    """
    # ops_team = "shiva.kher@gnani.ai"

    logger.info("Entering the function send_mail for the email : " + ops_team)
    try:
        with app.app_context():
            msg = Message(subject="Sending ALERT!! for an Exception in early salary welcome calling" ,
                          sender="vb.services@gnani.ai",
                          recipients=ops_team.split(","))
            msg.body = message + "  " + key

            mail.send(msg)     

    except BaseException as exception:
        logger.info(exception)
        logger.info("Unable to send the mail due to connectivity issue with the email server " + str(exception))

    logger.info("Exiting the function send_mail for the email : " + ops_team)

def send_converstaion_logs(message,key="",subj="",has_attachment=False,attachment_type='text/csv',attachment_path=''):
    """
        :pram: message - message to be sent (can have html)
        :pram: subj - subject of the email
        :pram: has_attachment - True/False
        :pram: attachment_type - text/csv
        :pram: attachment_path - path of the attachment
        :pram: is_html - True/False (if html content present in message body)
        :return: Json response from email api
    """
    url = "http://172.16.10.9:8080/alert"
    # Emails to send the mail
    team_email = ",".join(ops_team)
    # Bot name in case if you are working on different bots
    bot_name = "ES WELCOME CALLING"
    payload_general={'subject': f"{subj} {bot_name}",
    'to_list': team_email,
    'msg_body': message,
    }
    payload_files = {}
    files = []
    if has_attachment:
        payload_files = {
            'is_attachment': 'yes',
            'attachment_type': 'text/csv'
        }
        if attachment_path != '':
            file_name = attachment_path.split("/")[-1]
            files=[
                ('attach_file',(file_name,open(attachment_path,'rb'),'text/plain'))
                ]
    payload_html = {}
    # finding if html content is present in the message body
    html_pattern = re.compile("\<\w+\>|\<\/\w+\>")
    if re.findall(html_pattern,message):
        payload_html = {
            "is_html":"yes"
        }
    payload = {**payload_general, **payload_files,**payload_html}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload, files=files)
    logger.info(f"Email has been sent for {team_email} response is {response.text}")
    return (response.json())

    