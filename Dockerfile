FROM gnanivb.azurecr.io/rasa:1.10.16.v1.1
WORKDIR /nlp_server
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .
RUN mkdir -p /word_vector/
RUN mv kn_vectors_wiki_lg /word_vector/
COPY . .
# RUN ["/bin/bash", "train.sh"]
EXPOSE 5002
ENTRYPOINT ["/bin/bash", "docker-entrypoint.sh"]