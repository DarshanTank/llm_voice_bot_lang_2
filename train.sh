#!/bin/bash
rasa train --fixed-model-name model_llm_eng --config ./configs/config.yml --domain ./configs/domain.yml
mkdir -p models
cd ./models
tar xvf model_llm_eng.tar.gz
