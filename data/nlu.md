## intent:intent_affirm
- yes it is
- okay okay madam
- yaa
- yaa yaa
- haa
- yes sure
- yes 
- yes sir
- ok
- yes mam
- okay
- exactly
- yes exactly
- yeah exactly
- yaa exactly
- exactly sir
- exactly madam
- yes ok
- ok sir
- ok why note
- yes yes
- yes for sure
- yes madam
- yes maam
- say it
- yes say me
- sure
- haa haa
- yes sir say
- yes
- say that mam
- say it mam
- okay say
- okay say that mam
- yes say it man
- okay say mam
- say madam
- say it madam
- say mam
- say yes mam
- yes mom
- haa yes
- haa madame
- yes sir 
- yes please 
- sir ok
- mam ok
- Yash please
- yes man
- okay man
- hmm
- hmm hmm
- yes say madam
- oh yes
- and yes
- and yes tell me
- and yes madam
- and yes go on
- and yes speak on
- and yaa tell me
- and yaa 
- yes yes yes
- say yes maam
- say yes
- yess
- yes please
- yes yes tell


## intent:busy
- no i am busy
- no time now
- no i dont have time now
- i dont have any time
- no i wont have time now
- please call me later i am busy now
- call back later i do not have time now
- i am busy
- i am busy call me later
- do not call me i am busy
- i'm busy don't irritate
- call me later i am busy now
- call later i am very busy now
- no i am quite busy right now
- quite busy
- can you call me later i do not have time now i am so busy
- i don't have time i am very busy 
- i am working and busy
- can we have a call later as i am busy now
- no i am busy
- busy
- call me later friend i am busy with something else now
- do not call i stay busy the whole day
- i stay busy
- yes i am very busy
- i am somewhere else and also busy
- i am somewhere else so busy 
- busy because i am somewhere else
- am busy call me later
- i usually stay busy during this time
- i don't have time for this i am super busy
- madam today i dont have time i am occupied
- madam today i cant talk to you i am occupied
- i am working i am busy right now
- really busy
- im busy right now call me later
- dont call me today i am busy today
- totally busy
- no madam dont call today i am busy
- i will be busy i don't want to talk
- busy now call later
- call later please busy now
- busy now call me later madam
- sir call later busy now
- busy now madam call later
- please call later madam busy now
- hey busy now call later madam


## intent:right_time
- yes tell me what to talk about
- tell me what is the matter
- tell me what you want to talk about
- tell me what do you want to talk about
- tell me what you want to talk
- tell me what do you want to talk 
- yes tell me 
- yes tell me madam
- yes good time
- yes its good time
- oh ok cool
- yes good time yes tell me
- good time yes tell me
- yes madam this is good time you can tell me
- this is good time you can tell me
- yaa madam this is good time you can tell me
- yes madam tell me this is good time 
- yeah good time tell me
- yeah its god time tell me madam
- yep good time
- yes please go ahead
- yes correct time
- yes correct tell me
- okay correct time
- yaa correct tell me
- correct
- yes correct talk
- yaa correct
- ya this is right time
- this is the right time
- yes this is right time
- its right time
- its right time tell me
- it is right time tell me
- tell me yes
- yes tell me madam
- yes tell me maam
- tell me maam
- tell me tell me
- yes i can talk
- yet continue


## intent:affirm_person_speaking
- yes i need to talk to agent
- call agent
- i want to speak with someone
- i wanted to speak with someone
- can i speak to someone
- please connect me to a human
- i want to talk to a human
- please transfer the call to human
- can you connect me to a human
- i would like to speak to someone
- i would like to speak to an human
- i would like to speak with an agent
- i want to speak to someone
- can i speak with someone
- i want to talk to someone
- can i talk to some person who is not a bot
- yeah can i talk to some one who is not a bot
- i want to talk to someone who can understand it
- connect me with someone who knows this
- transfer this call to some one who can helps
- can you transfer the call
- can you please transfer the call to an agent
- can you transfer the call to an agent
- transfer the call to an agent
- yes i need to talk to agent
- can you transfer the call to live agent
- please transfer the call to live agent
- i need to talk to a live agent
- connect me to a live agent
- can you connect me to customer care executive
- customer care executive please
- customer care executive
- connect me to an executive
- could you please connect me to some one who can understand
- connect me to customer executive
- okay i need to talk to an agent 
- need to talk to a live agent
- transfer the call to a live agent
- i need an expert agent

## intent:out_of_scope
- tax payment
- vote in
- 10
- 12
- 30
- 16
- bicycle
- the rest of it too
- Police
- Sets of it
- employees
- es
- ese
- all the fruits is
- the fruits is
- fruits are
- they are
- madam they are
- columbus weather
- affiliated with
- minimal botcon kareha
- what is the impression
- property written by Will
- author of Amos
- the day you ride a bicycle
- near new session
- property written by Will
- should also drink in India
- spoke to all the police stations
- make me drink
- mr wanching tv
- mr m Running
- write jogging
- rakab won the match
- so
- it is so cold
- it is so hot



## intent:intent_repeat
- just repeat it again
- can you tell me again i mean repeat
- can you repeat again
- please repeat 1 more time
- repeat again   
- repeat it
- repeat it again
- please repeat
- repeat
- to repeat
- please repeat that
- repeat one more time
- and repeat once
- can you repeat
- did not hear please repeat
- did not hear properly can you repeat
- i did not hear repeat it 
- please tell again
- please repeat repeat
- please repeat it again
- sorry i did not hear please can repeat this
- sorry repeat again
- sorry can you repeat it again
- can you repeat it again
- can you say it again
- please say it again
- tell me again please
- sorry i did not get u please repeat that
- say again repeat
- excuse me can you repeat it again
- repeat it again for me madam
- i want you to repeat it again for me
- i want you to repeat it again
- please can you repeat
- can you speak back
- please tell one more time
- tell me once more
- say that again



## intent:intent_deny
- no ma am
- noo
- not intrested
- sorry not intrested
- no mam
- no yaar
- nope
- no i do not want to know 
- i do not want to know madam
- no i do not want to know mam
- i do not want to know man
- no way
- dont have
- not now
- not
- no no no
- no no
- no way
- no madam
- not possible
- no mam
- no no sir
- no madam i am telling you the same
- no please
- no
- no not possible
- nope sorry
- naa
- naa madam
- naa mam
- naa sir
- naaa
- na
- nah
- i said no
- i said no right 
- no i said
- no i said please understand
- i already said no
- i told you already no
- no i told you already
- i cant

## intent:dnd
- do not disturb me
- please do not disturb me
- dont disturb me
- please madam do not disturb me
- please stop it
- stop man
- dont call 
- leave us alone
- dont call us again leave us alone
- dont want
- stop calling
- please dont call me 
- dont call me
- stop calling me
- i dont want any call back
- dont call back 
- do not call me again and again
- i should not be getting another call
- i do not want to talk to you. don’t call me
- why are you troubling me now please stop calling me
- i do not want to talk to you please do not call me
- i do not want to talk to you please do not call me again
- do not call me in the future or else your number will have to be blocked
- do not call me in the future
- please do not call me again just delete my number
- why do i get calls again and again from this number even after i repeatedly said i do not want to talk
- you guys are troubling me a lot please do not call again
- i should not be getting a call again otherwise i will have to take action
- i should not be getting a call again otherwise i will  report you
- i should not be getting a call again otherwise i will block you
- what is this you keep calling me again and again i do not want your call please do not call me again
- do not keep calling again and again
- please stop calling me
- i am fed up with your calls do not call me again
- how many times do i have to tell you to stop calling?
- why do you call now i have been troubling you by calling dont call back
- i am fed up with you guys stop calling
- if you called again then you will see
- dont need to call everyday dont call
- what is your problem why are you calling again and again stop calling me
- i should not get calls from this number again if it comes i will definitely take action
- whats the problem why you are bothering me stop calling me
- madam what is the problem why are you troubling again and again now i should not get call from this number
- i should not get call back from this number
- whats the problem why are you calling again and again dont call
- delete my number from your record please dont call me again and again
- stop calling you again and again
- dont call me